const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const errorHandler = require('./middleware/error');
const cookieParser = require('cookie-parser');
const connectDB = require('./db/db');

// Load env vars
dotenv.config({ path: './config/config.env' });

// connect to Database
connectDB();

const auth = require('./router/auth');
// const rent = require('./router/rent');
const user = require('./router/user');

const app = express();

// Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// Cookie parser
app.use(cookieParser());
app.use(cors());

// Chemin des images
const uploadsDir = require('path').join(__dirname, '/uploads');
app.use(express.static(uploadsDir));

// Prefix Router
app.use('/api/auth', auth);
app.use('/api', user);
// app.use("/api/v1/rents", rent);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Serveur lancer en mode ${process.env.NODE_ENV} sur le port ${PORT}`.yellow.bold));

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
	console.log(`Error: ${err.message}`.red.bold);
	// Close server & exit process
	ServiceWorkerRegistration.close(() => process.exit(1));
});
