const express = require('express');
const { getUsers,  } = require('../controller/user');

const User = require('../models/User');

const router = express.Router();

// middleware protect routes
const { protect } = require('../middleware/auth');

router.get('/user', getUsers);

module.exports = router;
