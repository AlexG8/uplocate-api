const express = require("express");
const { createRent } = require("../controller/rent");

const Rent = require("../models/Rent");

const router = express.Router();

// middleware protect routes
const { protect } = require("../middleware/auth");
