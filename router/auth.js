const express = require('express');
const router = express.Router();
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');
const User = require('../models/User')
const jwt = require('jsonwebtoken');


// Controller auth.js
const { login, getMe, logout } = require('../controller/auth');

// middleware protect routes
const { protect } = require('../middleware/auth');

// File upload configuration
const storage = multer.diskStorage({
	destination: './uploads',
	filename: (req, file, callback) => {
		crypto.pseudoRandomBytes(16, (err, raw) => {
			if (err) return callback(err);
            lastUploadedImageName = raw.toString('hex') + path.extname(file.originalname);
            console.log('lastUploadedImageName', lastUploadedImageName);
            callback(null, lastUploadedImageName);
		});
	}
});


const upload = multer({storage: storage});

// router auth

// POST
router.post('/login', login);

router.post('/register/image', upload.single('userimage'), (req, res) => {
	if(!req.file.originalname.match(/\.(jpg|jpeg|png)$/)){
		return res.status(400).json({
			msg: 'Type de fichier autoriser: jpg jpeg png'
		});
	}

	res.status(201).send({
		filename: req.file.filename, file: req.file
	});
});

let lastUploadedImageName = "";

router.post('/register', (req, res) => {

	// Create user
	const user =  new User({
		...req.body, image: lastUploadedImageName
	});

		user.save((err, user) => {
			if (err) {
				return res.status(500).json(err);
			}
			sendTokenResponse(user, 200, res);
		})
	});


// GET
router.get('/logout', logout);
router.get('/me', protect, getMe);


// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
	// Create token
	const token = user.getSignedJwtToken();

	const options = {
		expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000), // cookie expiration 30days
		httpOnly: true
	};

	if (process.env.NODE_ENV === 'production') {
		// make cookie secure in prod
		options.secure = true;
	}

	res
		.status(statusCode)
		.cookie('token', token, options) // options(expiration ))
		.json({
			success: true,
			token
		});
};

module.exports = router;
