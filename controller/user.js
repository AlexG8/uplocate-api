const User = require('../models/User');
const asyncHandler = require('express-async-handler');

// @desc      Get all user
// @route     GET /api/user
// @access    Public
exports.getUsers = asyncHandler(async (req, res, next) => {
	User.find({}, (err, users) => {
		if (err) return err;

		if (users) {
			res.send(users);
		}
	});
});
