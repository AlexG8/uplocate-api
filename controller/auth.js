const User = require('../models/User');
const asyncHandler = require('express-async-handler');
// const { protect } = require("../middleware/auth");
const jwt = require('jsonwebtoken');


// @desc      Register user
// @route     POST /api/auth/register
// @access    Public
exports.register = asyncHandler(async (req, res) => {

	// Create user
	const user = await new User({
		...req.body, image: lastUploadedImageName
	});

		user.save((err, user) => {
			if (err) {
				return res.status(500).json(err);
			}
			sendTokenResponse(user, 200, res);
		})
	})
	

// @desc      Login user
// @route     POST /api/auth/login
// @access    Public
exports.login = asyncHandler(async (req, res, next) => {
	const { email, password } = req.body;

	// Validate email & password
	if (!email || !password) {
		return next(console.log('Entrer votre mail et mot de passe', 400));
	}

	// Check for user
	const user = await User.findOne({ email }).select('+password'); // inclus le password to check it (select: false in model)

	if (!user) {
		return next(console.log('Identifiant invalide', 401));
	}

	// Verifie si les mot de passe corresponde
	const isMatch = await user.matchPassword(password); // appel bcrypt from User.js

	if (!isMatch) {
		return next(console.log('Mot de passe invalide', 401));
	}
	// si tout est ok on créer le token

	sendTokenResponse(user, 200, res);
});

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
	// Create token
	const token = user.getSignedJwtToken();

	const options = {
		expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000), // cookie expiration 30days
		httpOnly: true
	};

	if (process.env.NODE_ENV === 'production') {
		// make cookie secure in prod
		options.secure = true;
	}

	res
		.status(statusCode)
		.cookie('token', token, options) // options(expiration ))
		.json({
			success: true,
			token
		});
};

// @desc      Log user out / clear cookie
// @route     GET /api/v1/auth/logout
// @access    Private
exports.logout = asyncHandler(async (req, res, next) => {
	res.cookie('token', 'none', {
		expires: new Date(Date.now() + 10 * 1000),
		httpOnly: true
	});

	res.status(200).json({
		success: true,
		data: {}
	});
});

// @desc      Get current logged in user
// @route     POST /api/auth/me
// @access    Private
exports.getMe = asyncHandler(async (req, res, next) => {
	const user = await User.findById(req.user.id);

	res.status(200).json({
		success: true,
		data: user
	});
});
