const Rent = require("../models/Rent");
const asyncHandler = require("express-async-handler");

exports.getRents = asyncHandler(async (req, res, next) => {
  try {
    const rents = await Rent.find();

    res.status(200).json({ success: true, data: rents });
  } catch (err) {
    res.status(400).json({ success: false });
  }
});

exports.getRent = asyncHandler(async (req, res, next) => {
  const rent = await Rent.findById(req.params.id);

  if (!bootcamp) {
    return next(
      console.log(
        `Le logement avec l'id ${req.params.id} n'a pas été trouver`,
        404
      )
    );
  }
  res.status(200).json({ success: true, data: rent });
});

exports.createRent = async (req, res, next) => {
  try {
    const rent = await Rent.create(req.body);

    res.status(201).json({
      success: true,
      data: rent
    });
  } catch (err) {
    res.status(400).json({ succes: false });
  }
};
