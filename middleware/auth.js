const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');
const User = require('../models/User');

// Protect Routes
exports.protect = asyncHandler(async (req, res, next) => {
	let token;

	if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
		token = req.headers.authorization.split(' ')[1]; // acces token
	}
	// else if(req.cookies.token) {
	//     token = req.cookies.token
	// }

	// Make sure token exists
	if (!token) {
		return next(console.log("Pas d'accés autoriser sur cette route", 401));
	}

	try {
		// Verify token
		const decoded = jwt.verify(token, process.env.JWT_SECRET);

		req.user = await User.findById(decoded.id);

		next();
	} catch (err) {
		return next(console.log("Pas d'accés autoriser sur cette route", 401));
	}
});
