const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

let userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "Ajouter votre nom"]
  },
  lastName: {
    type: String,
    required: [true, "Ajouter votre prenom"]
  },
  email: {
    type: String,
    required: true,
    unique: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Ajouter un mail valide"
    ]
  },
  password: {
    type: String,
    required: [true, "Ajouter un mot de passe"],
    minlength: 6,
    select: false
  },
  image: {
    type: String
    },
  resetPasswordToken: String,
  resetPasswordExpire: Date,
  createdAt: {
    type: Date,
    default: Date.now
  }
});

// crypt password with bcrypt
userSchema.pre("save", async function(next) {
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// JWT token
userSchema.methods.getSignedJwtToken = function() {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE
  });
};

// faire correspondre le mot de passe entré par l'utilisateur au mot de passe crypté dans la base de données
userSchema.methods.matchPassword = async function(enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

const User = mongoose.model("user", userSchema);
module.exports = User;
