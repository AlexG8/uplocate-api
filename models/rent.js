const mongoose = require("mongoose");

let rentSchema = new mongoose.Schema({
  adress: {
    type: String,
    required: [true, "Ajouter une adresse"],
    trim: true
  },
  startDate: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: [true, "Ajouter une adresse"],
    trim: true,
    minlength: [50, "Ajouter une description avec plus de 50 caracteres"],
    maxlength: [500, "Votre description est trop longue (500caracteres+)"]
  },
  photo: {
    type: String,
    default: "no-photo.jpg"
  },
  nbBedroom: {
    type: Number
  },
  rentPrice: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true
  }
});

const Rent = mongoose.model("rent", rentSchema);
module.exports = Rent;
